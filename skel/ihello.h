#pragma once
#include "dom.h"

struct IHello : public virtual Dom::IUnknown {
	virtual void Print() = 0;

	define_uiid(Hello)
};