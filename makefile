OUTPUT_DIR      = $(abspath $(shell pwd)/bin)
INSTALL_DIR     = /usr/local

all: libdom regsrv sample
	@mkdir -p ${OUTPUT_DIR}

libdom:
	@mkdir -p ${OUTPUT_DIR}
	@cd ./dom; make libdom OUTPUT_DIR=${OUTPUT_DIR}

regsrv:
	@mkdir -p ${OUTPUT_DIR}
	@cd ./dom; make regsrv OUTPUT_DIR=${OUTPUT_DIR}

sample:
	@mkdir -p ${OUTPUT_DIR}
	@cd ./dom; make sample OUTPUT_DIR=${OUTPUT_DIR}
	@cd ./skel; make OUTPUT_DIR=${OUTPUT_DIR}

install:
	@cd ./dom; make install INSTALL_DIR=${INSTALL_DIR}

clean:
	@echo "Cleaning..."
	@cd ./dom; make clean INSTALL_DIR=${INSTALL_DIR}