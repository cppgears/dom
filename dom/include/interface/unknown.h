#pragma once
#include <string>

#define dom_guid_pre_name	"guid-"
#define dom_cls_pre_name	"clsuid-"

#define define_uiid(name) \
	inline static const std::string& guid() { const static std::string idn(dom_guid_pre_name #name); return idn; }

#define define_clsuid(name) \
	inline static const std::string& guid() { const static std::string idn(dom_cls_pre_name #name); return idn; }

namespace Dom {
	using uiid = std::string;
	using clsuid = std::string;

	static inline const clsuid clsid(const char* class_name) { return (clsuid(dom_cls_pre_name) + clsuid(class_name)); }
	static inline const uiid iid(const char* interface_name) { return (uiid(dom_guid_pre_name) + uiid(interface_name)); }

	struct IUnknown
	{
		virtual long AddRef() = 0;
		virtual long Release() = 0;
		virtual bool QueryInterface(const uiid&, void **ppv) = 0;

		define_uiid(Unknown)
	};
}