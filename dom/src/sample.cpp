#ifdef	DOM_SAMPLE
#include <cstdio>
#include "../include/dom.h"

#include "../../skel/ihello.h"
int main()
{
	Dom::Interface<Dom::IUnknown>	unkwn;
	Dom::Interface<IHello>			hello;

	if (Dom::CreateInstance(Dom::clsid("SimpleHello"), unkwn)) {
		unkwn->QueryInterface(IHello::guid(), hello);
		hello->Print();
	}
	else {
		printf("[WARNING] Class `SimpleHello` not register.\nFirst execute command\n\tregsrv <fullpath>/libskel.so\n... and try again.");
	}

	return 0;
}
#endif

