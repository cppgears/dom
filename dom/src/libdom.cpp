#ifdef	DOM_LIBDOM

#if defined(__GNUC__)
//  GCC
#define EXPORT __attribute__((visibility("default")))
#define IMPORT
#endif

#include "../include/server.h"
#include "../include/interface/registryserver.h"
#include <dlfcn.h>
#include <atomic>
#include <mutex>
#include <unordered_map>
#include <cstring>

#include "include/dll.h"
#include "include/registerserver.h"

namespace Dom {

	constexpr const char* default_scope = "/opt/dom";

	static unordered_map<string, Dll >	DllServerList;
	static mutex						DllServerLock;

	EXPORT bool CreateInstance(const clsuid objid, void **ppIRequired, string scope) {
		if (scope.empty()) {
			scope = default_scope;
		}
		auto  target = CRegistryServer::target_so(objid, scope);
		unique_lock<mutex> lock(DllServerLock);

		if (!target.empty()) {
			try {
				auto itDll = DllServerList.find(target);
				if (itDll == DllServerList.end()) {
					itDll = DllServerList.emplace(target, Dll(target)).first;
					if (itDll == DllServerList.end()) {
						printf("[ DOM ] CreateInstance. Exception: %s\n", "Can not allocated.");
						return false;
					}
				}

				return itDll->second.CreateInstance(objid, ppIRequired);
			}
			catch (string ex) {
				printf("[ DOM ] CreateInstance. Exception: %s\n", ex.c_str());
			}
		}

		return false;
	}

	EXPORT vector<pair<string, string>> EnumObjects(string scope) {
		CRegistryServer registry("", scope.empty() ? default_scope : scope);
		return registry.Enum(false);
	}

	EXPORT bool RegisterServer(string sopathame, string scope) {
		CRegistryServer registry(sopathame, scope.empty() ? default_scope : scope);
		return registry.RegisterServer();
	}

	EXPORT bool UnRegisterServer(string sopathame, string scope) {
		CRegistryServer registry(sopathame, scope.empty() ? default_scope : scope);
		return registry.UnRegisterServer();
	}

}
#endif